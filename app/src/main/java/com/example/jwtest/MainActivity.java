package com.example.jwtest;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.longtailvideo.jwplayer.JWPlayerView;
import com.longtailvideo.jwplayer.configuration.PlayerConfig;
import com.longtailvideo.jwplayer.media.ads.AdBreak;
import com.longtailvideo.jwplayer.media.ads.ImaAdvertising;
import com.longtailvideo.jwplayer.media.playlists.PlaylistItem;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    JWPlayerView mPlayerView;
    TextView title, desc;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mPlayerView = findViewById(R.id.jwplayer);
        title = findViewById(R.id.title);
        desc = findViewById(R.id.desc);

        List<AdBreak> adSchedule = new ArrayList<>();

        AdBreak adBreak = new AdBreak.Builder()
                .tag("https://googleads.g.doubleclick.net/pagead/ads?ad_type=video&client=ca-video-pub-4968145218643279&videoad_start_delay=0&description_url=http%3A%2F%2Fwww.google.com&max_ad_duration=40000&adtest=on")
                .offset("pre")
                .build();

        adSchedule.add(adBreak);

        ImaAdvertising imaAdvertising = new ImaAdvertising(adSchedule);

        PlaylistItem playlistItem = new PlaylistItem.Builder()
                .file("https://cdn.jwplayer.com/manifests/mosnGcV6.m3u8")
                .build();

        String videoTitle = playlistItem.getTitle();
        String videoDesc = playlistItem.getDescription();

        Log.d("Metadata: ", String.valueOf(playlistItem.getExternalMetadata()));
        title.setText(videoTitle);
        desc.setText(videoDesc);

        List<PlaylistItem> playlist = new ArrayList<>();
        playlist.add(playlistItem);

        PlayerConfig config = new PlayerConfig.Builder()
                .playlist(playlist)
                .advertising(imaAdvertising)
                .autostart(true)
                .build();

        mPlayerView.setup(config);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mPlayerView.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPlayerView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mPlayerView.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPlayerView.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mPlayerView.onDestroy();
    }
}